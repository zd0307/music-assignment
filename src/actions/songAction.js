import {
    ADD_FAVOURITE_SONG,
    DELETE_FAVOURITE_SONG,


    FETCH_ALL_SONGS, FETCH_FAVOURITE_SONG,
    SELECT_SONG,
    songs_URL
} from "../helper";
import axios from "axios";

const fetchAllSongs = () => dispatch => {


    // axios.get(songs_URL)
    //     .then( res => {

    //         dispatch(
    //             {
    //                 //action的类型
    //                 type: FETCH_ALL_SONGS,
    //                 //获取的一个song list
    //                 payload: res.data.data
    //             }
    //         )
    //     })
    //     .catch( err => console.log(err))


    //axios..
    axios.get('10.0.0.94:3111/public/test')

}




const selectSong = songID => dispatch => {

    dispatch(
        {
            type: SELECT_SONG,
            //获取了一个song id
            payload: songID
        }
    )
}

const addFavSong = songId => dispatch => {


    dispatch(
        {
            type:ADD_FAVOURITE_SONG,
            payload:songId
        }
    )
}
const deleteFavSong = songId => dispatch => {
    dispatch(
        {
            type:DELETE_FAVOURITE_SONG,
            payload:songId
        }
    )
}
const fetchFavSong = () => dispatch =>{
    dispatch(
        {
            type:FETCH_FAVOURITE_SONG,
            info:"fetch fav song"

        }
    )
}

export {
    fetchAllSongs,
    selectSong,
    addFavSong,
    deleteFavSong,
    fetchFavSong
}
