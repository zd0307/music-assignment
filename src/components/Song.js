import {useDispatch, useSelector} from "react-redux";
import {ImHeadphones, ImHeart} from "react-icons/im";

import './Song.scss'
import {useEffect, useState} from "react";
import {Route, Routes, useNavigate} from "react-router-dom";
import {addFavSong, deleteFavSong} from "../actions/songAction";

export const Song = ({value}) => {

    //这里找action
    const dispatch = useDispatch()
    const [displayPrice,setDisplayPrice] = useState("none")
    const [displayBtn,setDisplayBtn] = useState("none")

    const navigate = useNavigate();
    const [fav,setFav] = useState(null)

    const songId = value.id;
    useEffect(()=>{
        if(value.fav)
        {
            setFav('addFavBtn')
        }
        else{
            setFav('notFavBtn')
        }
    },[])


    const btnHandler = () => {
        if(fav === 'addFavBtn'){
            setFav('notFavBtn')
            dispatch(deleteFavSong(songId))
        }else{
            setFav('addFavBtn')
            dispatch(addFavSong(songId))
        }

    }


    return(
        <div className='songContainer'
           onMouseEnter={()=>{
               setDisplayPrice("flex")
               setDisplayBtn("flex")
           }}
             onMouseLeave={()=>{
                 setDisplayPrice("none")
                 setDisplayBtn("none")
             }}
        >


            <img src={value.cover} alt=""/>


            <p>{value.title}</p>
            <p className='artist'>
                By: {value.artist}
            </p>


            {<div
                className='price' style={{display:displayPrice}}>
                ${value.price}
            </div> }

            {
                <div className='btnContainer' style={{display:displayBtn}}>
                    <button className='playNow' onClick={
                        ()=>{ navigate('/player')}
                    }
                    ><ImHeadphones/></button>


                </div>
            }

            <p className='length'>
                length: {value.length}
            </p>

             <button id= 'fa'
                style={{border:"none",
                    bottom:"3%",
                    right:"5%",
                    zIndex:"999",
                    fontSize:'large'
                }}

                onClick = {btnHandler}
                className= {fav}
                ><ImHeart/>
            </button>





        </div>
    )

}